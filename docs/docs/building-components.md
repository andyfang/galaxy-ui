## Styling with custom attributes

Galaxy uses the `g` attribute to style elements and `class` attribute to toggle states. This eliminates the need for prefixing, and creates semantic and more modular code.

#### Example Button

```
<button g="centered primary">Create Page</button>
```
Imagine you were creating a button for Facebook.com:
```
<button g="centered primary" fb="create_page">Create Page</button>
```
In this example, `centered primary` pertains to the Galaxy framework, while `create_page` describes the role of the button specific to Facebook.com.

It's not recommended to use class to style components, as this can create a redundant pile of 

#### Bad:

```
<button class="btn btn-centered btn-primary create_page">Create Page</button>
```

